/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QWEBOSSCREEN_H
#define QWEBOSSCREEN_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qwebosglobal_p.h"
#include <QtCore/QPointer>
#include <PDL.h>

#include <qpa/qplatformscreen.h>

#include <QtSensors/QOrientationReading>

QT_BEGIN_NAMESPACE

class QWebOSWindow;
class QOpenGLContext;
class QOrientationSensor;
class QWebOSIntegration;

class Q_WEBOS_EXPORT QWebOSScreen : public QObject, public QPlatformScreen
{
    Q_OBJECT

public:
    QWebOSScreen(QWebOSIntegration *integration);
    ~QWebOSScreen();

    QRect geometry() const override;
    virtual QRect rawGeometry() const;
    int depth() const override;
    QImage::Format format() const override;

    QSizeF physicalSize() const override;
    QDpi logicalDpi() const override;
    qreal pixelDensity() const override;
    Qt::ScreenOrientation nativeOrientation() const override;
    Qt::ScreenOrientation orientation() const override;

    qreal refreshRate() const override;

    QPixmap grabWindow(WId wid, int x, int y, int width, int height) const override;

    void *primarySurface() const { return m_surface; }
    bool m_rotateInProgress;
    bool swapAllowed() const;
    void rotateIfNeeded();

private:
    void setPrimarySurface(void *surface);
    void updateOrientation(QOrientationReading::Orientation currentOrientation);

    QPointer<QWindow> m_pointerWindow;
    void *m_surface;

    friend class QWebOSWindow;

    QWebOSIntegration *m_webosIntegration;
    QOrientationSensor *m_orientationSensor;
    Qt::ScreenOrientation m_screenOrientation;
    bool m_locked;
    QOrientationReading::Orientation m_lockOrientation;
    bool m_needRotate;
    PDL_Orientation m_needRotateOrient;
    QRect m_geometry;

private Q_SLOTS:
    void startSensor();
    void stopSensor();
    void orientationReadingChanged();
    void onStarted();
    static PDL_bool cb(PDL_ServiceParameters *parms, void *user);
};

QT_END_NAMESPACE

#endif // QWEBOSSCREEN_H
