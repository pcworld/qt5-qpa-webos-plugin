/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtSensors module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qwebosaccelerometer.h"
#include <QDebug>
#include <QtGlobal>

#define GRAVITY_CONST 9.8

char const * const QWebOSAccelerometer::id("webos.accelerometer");

QWebOSAccelerometer::QWebOSAccelerometer(QSensor *sensor)
    : QWebOSSensorCommon(sensor)
    , m_rotateReading(false)
{
    SDL_InitSubSystem(SDL_INIT_JOYSTICK);
    m_joystick = SDL_JoystickOpen(0);
    addDataRate(30, 30); // 30Hz
    setReading<QAccelerometerReading>(&m_reading);

    // On TouchPad raw sensor readings must be rotated to get correct values.
    // See https://github.com/woce/LunaSysMgr/blob/a8f94c9171da2af9cdf25a820af6f2a2d68123fc/Src/base/hosts/HostArmTopaz.cpp#L123
    PDL_ScreenMetrics outMetrics;
    PDL_GetScreenMetrics(&outMetrics);
    if (outMetrics.horizontalPixels > outMetrics.verticalPixels)
        m_rotateReading = true;
}

QWebOSAccelerometer::~QWebOSAccelerometer()
{
    if (SDL_JoystickOpened(0))
        SDL_JoystickClose(m_joystick);
    SDL_QuitSubSystem(SDL_INIT_JOYSTICK);
}

void QWebOSAccelerometer::poll()
{
    float xAxisForceInGs = (float) SDL_JoystickGetAxis(m_joystick, 0) / 32768.0;
    float yAxisForceInGs = (float) SDL_JoystickGetAxis(m_joystick, 1) / 32768.0;
    float zAxisForceInGs = (float) SDL_JoystickGetAxis(m_joystick, 2) / 32768.0;

    m_reading.setTimestamp(getTimestamp());

    if (m_rotateReading) {
        // touchpad
        m_reading.setX(-yAxisForceInGs*GRAVITY_CONST);
        m_reading.setY(-xAxisForceInGs*GRAVITY_CONST);
        m_reading.setZ( zAxisForceInGs*GRAVITY_CONST);
    } else {
        // pre3
        m_reading.setX(-xAxisForceInGs*GRAVITY_CONST);
        m_reading.setY( yAxisForceInGs*GRAVITY_CONST);
        m_reading.setZ( zAxisForceInGs*GRAVITY_CONST);
    }

    newReadingAvailable();
}

