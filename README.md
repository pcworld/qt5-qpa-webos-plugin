Qt5 QPA webOS plugin
====================

Build requirements
------------------
- Linux PC
- PalmSDK
- qt5-webos-sdk

How to build webos plugin
-------------------------

    cd webos
    qmake-webos -o Makefile webos.pro
    make
    make install

