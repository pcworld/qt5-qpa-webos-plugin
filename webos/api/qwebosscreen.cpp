/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtCore/qtextstream.h>
#include <QtGui/qwindow.h>
#include <qpa/qwindowsysteminterface.h>
#include "qwebosopenglcompositor_p.h"
#include <private/qmath_p.h>
#include "qwebosintegration_p.h"

#include "qwebosscreen_p.h"
#include "qweboswindow_p.h"

#include <QGuiApplication>
#include <QtSensors/QOrientationSensor>

#include <SDL.h>

QT_BEGIN_NAMESPACE

QWebOSScreen::QWebOSScreen(QWebOSIntegration *integration)
    : m_rotateInProgress(false)
    , m_surface(NULL)
    , m_webosIntegration(integration)
    , m_orientationSensor(new QOrientationSensor(this))
    , m_screenOrientation(nativeOrientation())
    , m_locked(false)
    , m_lockOrientation(QOrientationReading::TopUp)
    , m_needRotate(false)
    , m_needRotateOrient(PDL_ORIENTATION_0)
    , m_geometry(rawGeometry())
{
    if (!qEnvironmentVariableIsEmpty("QT_QPA_WEBOS_AUTOROTATE")) {
        connect(m_orientationSensor, SIGNAL(readingChanged()), this, SLOT(orientationReadingChanged()));
        QTimer::singleShot(0, this, SLOT(onStarted()));
    }
    else {
        QRect r = rawGeometry();

        static int rotation = qEnvironmentVariableIntValue("QT_QPA_WEBOS_ROTATION");
        switch (rotation) {
        case 0:
        case 180:
        case -180:
            break;
        case 90:
        case -90: {
            int h = r.height();
            r.setHeight(r.width());
            r.setWidth(h);
            break;
        }
        default:
            qWarning("Invalid rotation %d specified in QT_QPA_WEBOS_ROTATION", rotation);
            break;
        }
        m_geometry = r;
    }
}

QWebOSScreen::~QWebOSScreen()
{
    PDL_UnregisterServiceCallback(QWebOSScreen::cb);
    if (m_orientationSensor) {
        m_orientationSensor->stop();
        delete m_orientationSensor;
        m_orientationSensor = NULL;
    }
    QOpenGLCompositor::destroy();
}

bool QWebOSScreen::swapAllowed() const
{
    // An attempt to synchronize webOS screen rotation graphical effect on TP
    // with actual screen update by qt plugin
    static QSize screenSize = rawGeometry().size();
    static bool isTouchpad = (screenSize.width() > screenSize.height());
    if (isTouchpad)
        return ((!m_rotateInProgress) || (QGuiApplication::applicationState() == Qt::ApplicationInactive));
    else
        return true;
}

void QWebOSScreen::rotateIfNeeded()
{
    if (m_needRotate)
        PDL_SetOrientation(m_needRotateOrient);
    m_needRotate = false;
}

void QWebOSScreen::startSensor()
{
    if (!m_orientationSensor) return;
    m_orientationSensor->start();
    orientationReadingChanged();
}

void QWebOSScreen::stopSensor()
{
    if (!m_orientationSensor) return;
    m_orientationSensor->stop();
}

PDL_bool QWebOSScreen::cb(PDL_ServiceParameters *parms, void *user)
{
    QWebOSScreen *s = static_cast<QWebOSScreen*>(user);
    if (PDL_TRUE != PDL_ParamExists(parms, "rotationLock")) return PDL_TRUE;

    int rotationLock = PDL_GetParamInt(parms, "rotationLock");
    switch (rotationLock) {
    case 0:
        s->m_locked = false;
        break;
    case 3:
        s->m_lockOrientation = QOrientationReading::TopUp;
        s->m_locked = true;
        break;
    case 4:
        s->m_lockOrientation = QOrientationReading::TopDown;
        s->m_locked = true;
        break;
    case 5:
        s->m_lockOrientation = QOrientationReading::RightUp;
        s->m_locked = true;
        break;
    case 6:
        s->m_lockOrientation = QOrientationReading::LeftUp;
        s->m_locked = true;
        break;
    }

    if (s->m_locked) {
	QTimer::singleShot(0, s, SLOT(stopSensor()));
        s->updateOrientation(s->m_lockOrientation);
    }
    else {
	QTimer::singleShot(0, s, SLOT(startSensor()));
    }

    return PDL_TRUE;
}

void QWebOSScreen::onStarted()
{
    m_orientationSensor->start();

    PDL_ServiceCallWithCallback("palm://com.palm.systemservice/getPreferences", "{\"subscribe\":true, \"keys\": [ \"rotationLock\" ]}", QWebOSScreen::cb, this, PDL_FALSE);

    QGuiApplication::primaryScreen()->setOrientationUpdateMask(Qt::LandscapeOrientation | Qt::PortraitOrientation | Qt::InvertedLandscapeOrientation | Qt::InvertedPortraitOrientation);
}

QRect QWebOSScreen::geometry() const
{
    return m_geometry;
}

QRect QWebOSScreen::rawGeometry() const
{
    const SDL_VideoInfo* info = SDL_GetVideoInfo();
    const int defaultWidth = info->current_w;
    const int defaultHeight = info->current_h;

    static QSize size;

    if (size.isEmpty()) {
        int width = qEnvironmentVariableIntValue("QT_QPA_WEBOS_WIDTH");
        int height = qEnvironmentVariableIntValue("QT_QPA_WEBOS_HEIGHT");

        if (width && height) {
            size.setWidth(width);
            size.setHeight(height);
        }
        else {
            size.setWidth(defaultWidth);
            size.setHeight(defaultHeight);
        }
    }
    return QRect(QPoint(0, 0), size);
}

int QWebOSScreen::depth() const
{
    const SDL_VideoInfo* info = SDL_GetVideoInfo();
    const int defaultDepth = info->vfmt->BitsPerPixel;
    static int depth = qEnvironmentVariableIntValue("QT_QPA_WEBOS_DEPTH");

    if (depth == 0) {
        depth = defaultDepth;
    }
    return depth;
}

QImage::Format QWebOSScreen::format() const
{
    return depth() == 16 ? QImage::Format_RGB16 : QImage::Format_RGB32;
}

QSizeF QWebOSScreen::physicalSize() const
{
    const int defaultPhysicalDpi = 100;
    static QSizeF size;

    if (size.isEmpty()) {
        // Note: in millimeters
        int width = qEnvironmentVariableIntValue("QT_QPA_WEBOS_PHYSICAL_WIDTH");
        int height = qEnvironmentVariableIntValue("QT_QPA_WEBOS_PHYSICAL_HEIGHT");

        if (width && height) {
            size.setWidth(width);
            size.setHeight(height);
        }
        else {
            size.setWidth(screen()->size().width() * Q_MM_PER_INCH / defaultPhysicalDpi);
            size.setHeight(screen()->size().height() * Q_MM_PER_INCH / defaultPhysicalDpi);
            qWarning("Unable to query physical screen size, defaulting to %d dpi.\n"
                     "To override, set QT_QPA_WEBOS_PHYSICAL_WIDTH "
                     "and QT_QPA_WEBOS_PHYSICAL_HEIGHT (in millimeters).", defaultPhysicalDpi);
        }
    }

    return size;
}

QDpi QWebOSScreen::logicalDpi() const
{
    const QSizeF ps = physicalSize();
    const QSize s = screen()->size();

    if (!ps.isEmpty() && !s.isEmpty())
        return QDpi(25.4 * s.width() / ps.width(),
                    25.4 * s.height() / ps.height());
    else
        return QDpi(100, 100);
}

qreal QWebOSScreen::pixelDensity() const
{
    return qMax(1, qRound(logicalDpi().first / qreal(100)));
}

Qt::ScreenOrientation QWebOSScreen::nativeOrientation() const
{
    return (rawGeometry().height() > rawGeometry().width())? Qt::PortraitOrientation: Qt::LandscapeOrientation;
}

Qt::ScreenOrientation QWebOSScreen::orientation() const
{
    return m_screenOrientation;
}

qreal QWebOSScreen::refreshRate() const
{
    return 60;
}

void QWebOSScreen::setPrimarySurface(void *surface)
{
    m_surface = surface;
}

QPixmap QWebOSScreen::grabWindow(WId wid, int x, int y, int width, int height) const
{
    QOpenGLCompositor *compositor = QOpenGLCompositor::instance();
    const QList<QOpenGLCompositorWindow *> windows = compositor->windows();
    Q_ASSERT(!windows.isEmpty());

    QImage img;

    if (static_cast<QWebOSWindow *>(windows.first()->sourceWindow()->handle())->isRaster()) {
        // Request the compositor to render everything into an FBO and read it back. This
        // is of course slow, but it's safe and reliable. It will not include the mouse
        // cursor, which is a plus.
        img = compositor->grab();
    } else {
        // Just a single OpenGL window without compositing. Do not support this case for now. Doing
        // glReadPixels is not an option since it would read from the back buffer which may have
        // undefined content when calling right after a swapBuffers (unless preserved swap is
        // available and enabled, but we have no support for that).
        qWarning("grabWindow: Not supported for non-composited OpenGL content. Use QQuickWindow::grabWindow() instead.");
        return QPixmap();
    }

    if (!wid) {
        const QSize screenSize = geometry().size();
        if (width < 0)
            width = screenSize.width() - x;
        if (height < 0)
            height = screenSize.height() - y;
        return QPixmap::fromImage(img).copy(x, y, width, height);
    }

    foreach (QOpenGLCompositorWindow *w, windows) {
        const QWindow *window = w->sourceWindow();
        if (window->winId() == wid) {
            const QRect geom = window->geometry();
            if (width < 0)
                width = geom.width() - x;
            if (height < 0)
                height = geom.height() - y;
            QRect rect(geom.topLeft() + QPoint(x, y), QSize(width, height));
            rect &= window->geometry();
            return QPixmap::fromImage(img).copy(rect);
        }
    }
    return QPixmap();
}

void QWebOSScreen::orientationReadingChanged()
{
    if (m_locked) return;
    updateOrientation(m_orientationSensor->reading()->orientation());
}

void QWebOSScreen::updateOrientation(QOrientationReading::Orientation currentOrientation)
{
    QOpenGLCompositor *compositor = QOpenGLCompositor::instance();

    switch (currentOrientation) {
    case QOrientationReading::TopUp:   /* 0° */
        m_screenOrientation = nativeOrientation();
        m_geometry = rawGeometry();
        compositor->setRotation(0);
        m_needRotateOrient = PDL_ORIENTATION_0;
        break;
    case QOrientationReading::LeftUp:  /* 90° clockwise */
        m_screenOrientation = nativeOrientation() == Qt::PortraitOrientation ? Qt::InvertedLandscapeOrientation : Qt::PortraitOrientation;
        m_geometry = rawGeometry().transposed();
        compositor->setRotation(90);
        m_needRotateOrient = PDL_ORIENTATION_90;
        break;
    case QOrientationReading::TopDown: /* 180° */
        m_screenOrientation = nativeOrientation() == Qt::PortraitOrientation ? Qt::InvertedPortraitOrientation : Qt::InvertedLandscapeOrientation;
        m_geometry = rawGeometry();
        compositor->setRotation(180);
        m_needRotateOrient = PDL_ORIENTATION_180;
        break;
    case QOrientationReading::RightUp: /* 270° clockwise */
        m_screenOrientation = nativeOrientation() == Qt::PortraitOrientation ? Qt::LandscapeOrientation : Qt::InvertedPortraitOrientation;
        m_geometry = rawGeometry().transposed();
        compositor->setRotation(270);
        m_needRotateOrient = PDL_ORIENTATION_270;
        break;
    default:
        break;
    }

    if (m_screenOrientation == QPlatformScreen::screen()->orientation()) return;
    QWindowSystemInterface::handleScreenOrientationChange(QPlatformScreen::screen(), m_screenOrientation);
    m_rotateInProgress = true;
    if (geometry() != screen()->geometry()) {
        QWindowSystemInterface::handleScreenGeometryChange(QPlatformScreen::screen(), geometry(), geometry());
        resizeMaximizedWindows();
    }
    m_webosIntegration->requestUpdate();
    m_needRotate = true;
}

QT_END_NAMESPACE
